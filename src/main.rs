#![feature(generators, generator_trait)]
#![feature(is_sorted)]
#![feature(core_intrinsics)]
use core::arch::x86_64::{_mm_prefetch, _MM_HINT_NTA};
use std::ops::{Generator, GeneratorState};
use std::pin::Pin;
use std::slice;
use std::time::Instant;

use rand::{rngs::StdRng, Rng, SeedableRng};
use structopt::StructOpt;

#[derive(Debug, Clone, StructOpt)]
#[structopt(name = "Mega Binary Search")]
struct Opts {
    /// Random seed.
    #[structopt(long, default_value = "0")]
    seed: u64,
    /// Core.
    #[structopt(long, default_value = "6")]
    core: usize,
    /// Huge vector length.
    #[structopt(short, long, default_value = "67108864")]
    huge_len: usize,
    /// Small vector length.
    #[structopt(short, long, default_value = "1048576")]
    small_len: usize,
    /// The algorithm to use.
    #[structopt(subcommand)]
    algo: Algorithm,
    /// Mega binary search pipeline width.
    #[structopt(short, long, default_value = "8")]
    width: usize,
    /// Number of repeats.
    #[structopt(short, long, default_value = "5")]
    repeat: usize,
}

#[derive(Debug, Clone, StructOpt)]
enum Algorithm {
    /// Naive approach
    Naive,
    /// Handcrafted state machine
    Sm,
    /// Generator
    Mega,
}

#[inline]
fn binary_search(value: usize, arr: &[usize]) -> usize {
    let mut l = 0;
    let mut r = arr.len();
    while l + 1 < r {
        let mid = (l + r) / 2;
        if value < unsafe { *arr.get_unchecked(mid) } {
            r = mid;
        } else {
            l = mid;
        }
    }
    if value < arr[l] {
        l
    } else {
        r
    }
}

fn baseline(_opts: &Opts, small: &[usize], huge: &[usize]) {
    for &x in small {
        let _pos = binary_search(x, huge);
        // let _pos = huge.binary_search(&x);
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum State {
    Continue,
    Complete(usize),
    Empty,
}

impl Default for State {
    fn default() -> Self {
        State::Empty
    }
}

#[derive(Debug, Default)]
struct Frame<'a> {
    huge: &'a [usize],
    first: usize,
    middle: usize,
    len: usize,
    half: usize,
    val: usize,
    state: State,
}


impl<'a> Frame<'a> {
    fn init(&mut self, huge: &'a [usize], key: usize) {
        self.val = key;
        self.huge = huge;
        self.first = 0;
        self.len = huge.len();
        if huge.is_empty() {
            self.state = State::Complete(0);
            return;
        }

        self.half = self.len / 2;
        self.middle = self.first + self.half;
        self.state = State::Continue;
        unsafe { _mm_prefetch::<{ _MM_HINT_NTA }>(self.huge.as_ptr().add(self.middle) as _) };
    }

    fn run(&mut self) -> State {
        let x = unsafe { *self.huge.get_unchecked(self.middle) };
        if x < self.val {
            self.first = self.middle;
            self.first += 1;
            self.len -= self.half + 1;
        } else {
            self.len = self.half;
        }

        if self.len > 0 {
            self.half = self.len / 2;
            self.middle = self.first + self.half;
            unsafe { _mm_prefetch::<{ _MM_HINT_NTA }>(self.huge.as_ptr().add(self.middle) as _) };
            return State::Continue;
        }

        self.state = State::Complete(self.middle);
        self.state
    }
}

fn state_machine_binary_search(opts: &Opts, small: &[usize], huge: &[usize]) {
    let mut frames: Vec<Frame> = Vec::with_capacity(opts.width);
    frames.resize_with(frames.capacity(), Default::default);
    let n = opts.width - 1;
    let mut i = n;

    for &key in small {
        let mut fr = &mut frames[i];
        if fr.state != State::Continue {
            fr.init(&huge, key);
            if i == 0 { i = n; } else { i -= 1; }
        } else {
            loop {
                if let State::Complete(_) = fr.run() {
                    fr.init(&huge, key);
                    if i == 0 { i = n; } else { i -= 1; }
                    break;
                }
                if i == 0 { i = n; } else { i -= 1; }
                fr = &mut frames[i];
            }
        }
    }

    loop {
        let mut more_work = false;
        for fr in &mut frames {
            if fr.state == State::Continue {
                more_work = true;
                fr.run();
            }
        }
        if !more_work { break; }
    }
}

fn mega_binary_search(value: usize, huge: &[usize]) -> impl Generator<Yield = (), Return = usize> {
    let arr = unsafe { slice::from_raw_parts(huge.as_ptr(), huge.len()) };
    // move || {
    //     let mut first = 0;
    //     let mut len = arr.len();
    //     while len > 0 {
    //         let half = len / 2;
    //         let middle = first + half;
    //         if len > 8 {
    //             unsafe { _mm_prefetch::<{ _MM_HINT_NTA }>(arr.as_ptr().add(middle) as _) };
    //             yield ();
    //         }
    //         let middle_key = unsafe { *arr.get_unchecked(middle) };
    //         if middle_key < value {
    //             first = middle + 1;
    //             len -= half + 1;
    //         } else {
    //             len = half;
    //         }
    //     }
    //     first
    // }

    move || {
        let mut l = 0;
        let mut r = arr.len();
        while l + 1 < r {
            let mid = (l + r) / 2;
            if r - l > 8 {
                unsafe { _mm_prefetch::<{ _MM_HINT_NTA }>(arr.as_ptr().add(mid) as _) };
                yield ();
            }
            if value < unsafe { *arr.get_unchecked(mid) } {
                r = mid;
            } else {
                l = mid;
            }
        }
        if value < arr[l] {
            l
        } else {
            r
        }
    }
}

fn mega_search(opts: &Opts, small: &[usize], huge: &[usize]) {
    let width = opts.width.min(small.len());
    let mut generators = Vec::with_capacity(width);
    for &x in &small[..width] {
        let generator = mega_binary_search(x, huge);
        generators.push(generator);
    }
    let mut next = width;
    while !generators.is_empty() {
        for i in 0..generators.len() {
            match Pin::new(&mut generators[i]).resume(()) {
                GeneratorState::Yielded(_) => {}
                GeneratorState::Complete(_pos) => {
                    if next == small.len() {
                        generators.swap_remove(i);
                        break;
                    }
                    generators[i] = mega_binary_search(small[next], huge);
                    next += 1;
                }
            }
        }
    }
}

fn bench<F>(f: &F, opts: &Opts, small: &[usize], huge: &[usize])
where
    F: Fn(&Opts, &[usize], &[usize])
{
    let start = Instant::now();
    for _ in 0..opts.repeat {
        f(opts, small, huge);
    }
    let dura = start.elapsed();
    println!(
        "{:.02} ns per lookup/log2(array-size)",
        dura.as_nanos() as f64
            / small.len() as f64
            / (huge.len() as f64).log2()
            / opts.repeat as f64
    );
}

fn main() {
    let opts = Opts::from_args();
    // scheduler::set_self_affinity(scheduler::CpuSet::single(opts.core)).unwrap();
    let mut small_vec = Vec::with_capacity(opts.small_len);
    let mut rng = StdRng::seed_from_u64(opts.seed);
    small_vec.resize_with(opts.small_len, || rng.gen_range(0..opts.huge_len * 2));
    let huge_vec: Vec<_> = (0..opts.huge_len).map(|x| x + x).collect();
    match opts.algo {
        Algorithm::Naive => bench(&baseline, &opts, &small_vec, &huge_vec),
        Algorithm::Sm => bench(&state_machine_binary_search, &opts, &small_vec, &huge_vec),
        Algorithm::Mega => bench(&mega_search, &opts, &small_vec, &huge_vec),
    }
}
